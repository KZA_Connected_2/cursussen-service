package nl.kzaconnected.cursusservice.service;

import nl.kzaconnected.cursusservice.model.Dao.Attitude;
import nl.kzaconnected.cursusservice.repository.AttitudeRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
public class AttitudeServiceTest {

    @TestConfiguration
    static class AttitudeServiceTestContextConfiguration {

        @Bean
        public AttitudeService attitudeService() {
            return new AttitudeService();
        }
    }

    @Autowired
    private AttitudeService attitudeService;
    @MockBean
    private AttitudeRepository attitudeRepository;

    private List<Attitude> attitudes;

    @Before
    public void init() {
        attitudes = new ArrayList<Attitude>();
        attitudes.add(Attitude.builder().id(10L).attitude("Attitude1").build());
        attitudes.add(Attitude.builder().id(10L).attitude("Attitude1").build());
    }

    @Test
    public void findAllAttitudesShouldReturnCorrectListOfAttitudes() {
        Mockito.when(attitudeRepository.findAll())
                .thenReturn(attitudes);
        List<Attitude> found = attitudeService.findAll();
        for (Attitude attitude : found) {
            Assert.assertTrue(attitudes.stream().anyMatch(attitude::equals));
        }
    }
}