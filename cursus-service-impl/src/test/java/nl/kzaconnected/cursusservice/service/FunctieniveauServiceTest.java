package nl.kzaconnected.cursusservice.service;

import nl.kzaconnected.cursusservice.model.Dao.Functieniveau;
import nl.kzaconnected.cursusservice.repository.FunctieniveauRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
public class FunctieniveauServiceTest {

    @TestConfiguration
    static class FunctieniveauServiceTestContextConfiguration {

        @Bean
        public FunctieniveauService functieniveauService() {
            return new FunctieniveauService();
        }
    }

    @Autowired
    private FunctieniveauService functieniveauService;
    @MockBean
    private FunctieniveauRepository functieniveauRepository;

    private List<Functieniveau> functieniveaus;

    @Before
    public void init() {
        functieniveaus = new ArrayList<Functieniveau>();
        functieniveaus.add(Functieniveau.builder().id(1L).functieniveau("Functieniveau1").build());
        functieniveaus.add(Functieniveau.builder().id(2L).functieniveau("Functieniveau2").build());
    }

    @Test
    public void getAllFunctieniveausShouldReturnCorrectListOfFunctieniveaus() {
        Mockito.when(functieniveauRepository.findAll())
                .thenReturn(functieniveaus);
        List<Functieniveau> found = functieniveauService.findAll();
        for (Functieniveau functieniveau : found) {
            Assert.assertTrue(functieniveaus.stream().anyMatch(functieniveau::equals));
        }
    }
}