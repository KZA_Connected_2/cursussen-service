package nl.kzaconnected.cursusservice.service;

import nl.kzaconnected.cursusservice.model.Dto.CursusDto;
import nl.kzaconnected.cursusservice.model.Dao.*;
import nl.kzaconnected.cursusservice.repository.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class CursusServiceTest {

    @TestConfiguration
    static class CursusServiceTestContextConfiguration {

        @Bean
        public CursusService cursusService() {
            return new CursusService();
        }
    }

    @Autowired
    private CursusService cursusService;
    @MockBean
    private CursusRepository cursusRepository;
    @MockBean
    private AttitudeRepository attitudeRepository;
    @MockBean
    private SlagingscriteriumRepository slagingscriteriumRepository;
    @MockBean
    private FunctieniveauRepository functieniveauRepository;
    @MockBean
    private StatusRepository statusRepository;

    private List<CursusDto> cursusDtos;
    private CursusDto cursusDto;
    private List<Cursus> cursussen;
    private Cursus cursus;
    private Attitude attitude;
    private Slagingscriterium slagingscriterium;
    private Functieniveau functieniveau;
    private Status status;

    @Before
    public void init() throws Exception{
        attitude = Attitude.builder().id(1L).attitude("Attitude1").build();
        slagingscriterium = Slagingscriterium.builder().id(1L).slagingscriterium("Slagingscriterium1").build();
        functieniveau = Functieniveau.builder().id(1L).functieniveau("Functieniveau1").build();
        status = Status.builder().id(1L).status("Status1").build();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        List<Datum> cursusData = new ArrayList<>();
        cursusData.add(Datum.builder().datum(simpleDateFormat.parse("01-01-2018 08:00:00")).build());
        cursusData.add(Datum.builder().datum(simpleDateFormat.parse("01-01-2019 08:00:00")).build());

        List<Docent> cursusDocenten = new ArrayList<>();
        cursusDocenten.add(Docent.builder().naam("Docent1").build());
        cursusDocenten.add(Docent.builder().naam("Docent2").build());

        cursusDto = CursusDto.builder()
                .id(1L)
                .naam("Cursus1")
                .attitude(attitude.getAttitude())
                .slagingscriterium(slagingscriterium.getSlagingscriterium())
                .functieniveau(functieniveau.getFunctieniveau())
                .status(status.getStatus())
                .cursusdata(cursusData)
                .cursusdocenten(cursusDocenten)
                .build();

        CursusDto cursusDto2 = cursusDto;
        cursusDto2.setId(2L);
        cursusDto2.setNaam("Cursus2");

        cursusDtos = new ArrayList<>();
        cursusDtos.add(cursusDto);
        cursusDtos.add(cursusDto2);

        cursus = Cursus.builder()
                .id(1L)
                .naam("Cursus1")
                .attitude(attitude)
                .slagingscriterium(slagingscriterium)
                .functieniveau(functieniveau)
                .status(status)
                .cursusdata(cursusData)
                .cursusdocenten(cursusDocenten)
                .build();

        Cursus cursus2 = cursus;
        cursus2.setId(2L);
        cursus2.setNaam("Cursus2");

        cursussen = new ArrayList<>();
        cursussen.add(cursus);
        cursussen.add(cursus2);
    }

    @Test
    public void findOneShouldReturnCursusDto() {
        Mockito.when(cursusRepository.findOne(1L))
                .thenReturn(cursus);
        CursusDto found = cursusService.findOne(1L);
        Assert.assertEquals(found,cursusDto);
    }

    @Test
    public void findAllShouldReturnListOfCursusDtos() {
        Mockito.when(cursusRepository.findAll())
                .thenReturn(cursussen);
        List<CursusDto> found = cursusService.findAll();
        for (CursusDto cursusDto : found) {
            Assert.assertTrue(cursusDtos.stream().anyMatch(cursusDto::equals));
        }
    }

    @Test
    public void saveShouldReturnCursusDto() {
        Mockito.when(cursusRepository.save(cursus))
                .thenReturn(cursus);
        Mockito.when(attitudeRepository.findByAttitude(attitude.getAttitude()))
                .thenReturn(attitude);
        Mockito.when(slagingscriteriumRepository.findBySlagingscriterium(slagingscriterium.getSlagingscriterium()))
                .thenReturn(slagingscriterium);
        Mockito.when(functieniveauRepository.findByFunctieniveau(functieniveau.getFunctieniveau()))
                .thenReturn(functieniveau);
        Mockito.when(statusRepository.findByStatus(status.getStatus()))
                .thenReturn(status);
        CursusDto found = cursusService.save(cursusDto);
        Assert.assertEquals(found,cursusDto);
    }

    @Test
    public void deleteShouldCallRepositoryMethodDelete(){
        cursusService.delete(1L);
        verify(cursusRepository).delete(1L);
    }
}