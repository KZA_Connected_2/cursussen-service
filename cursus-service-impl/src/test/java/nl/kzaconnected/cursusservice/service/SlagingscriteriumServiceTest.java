package nl.kzaconnected.cursusservice.service;

import nl.kzaconnected.cursusservice.model.Dao.Slagingscriterium;
import nl.kzaconnected.cursusservice.repository.SlagingscriteriumRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
public class SlagingscriteriumServiceTest {

    @TestConfiguration
    static class SlagingscriteriumServiceTestContextConfiguration {

        @Bean
        public SlagingscriteriumService slagingscriteriumService() {
            return new SlagingscriteriumService();
        }
    }

    @Autowired
    private SlagingscriteriumService slagingscriteriumService;
    @MockBean
    private SlagingscriteriumRepository slagingscriteriumRepository;

    private List<Slagingscriterium> slagingscriteria;

    @Before
    public void init() {
        slagingscriteria = new ArrayList<Slagingscriterium>();
        slagingscriteria.add(Slagingscriterium.builder().id(1L).slagingscriterium("Slagingscriterium1").build());
        slagingscriteria.add(Slagingscriterium.builder().id(2L).slagingscriterium("Slagingscriterium2").build());
    }

    @Test
    public void getAllSlagingscriteriaShouldReturnCorrectListOfSlagingscriteria() {
        Mockito.when(slagingscriteriumRepository.findAll())
                .thenReturn(slagingscriteria);
        List<Slagingscriterium> found = slagingscriteriumService.findAll();
        for (Slagingscriterium slagingscriterium : found) {
            Assert.assertTrue(slagingscriteria.stream().anyMatch(slagingscriterium::equals));
        }
    }
}