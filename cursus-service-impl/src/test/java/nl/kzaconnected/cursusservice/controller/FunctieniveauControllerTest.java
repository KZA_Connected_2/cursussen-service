package nl.kzaconnected.cursusservice.controller;

import nl.kzaconnected.cursusservice.model.Dao.Functieniveau;
import nl.kzaconnected.cursusservice.service.FunctieniveauService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(FunctieniveauController.class)
public class FunctieniveauControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private FunctieniveauService functieniveauService;

    @Test
    public void getAllFunctieniveausShouldReturn200OkAndJsonArray() throws Exception {
        List<Functieniveau> functieniveaus = Arrays.asList(new Functieniveau(),new Functieniveau());
        Mockito.when(functieniveauService.findAll())
                .thenReturn(functieniveaus);
        mockMvc.perform(get("/functieniveaus")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].*", hasSize(2)))
                .andExpect(jsonPath("$[1].*", hasSize(2)));
    }
}