package nl.kzaconnected.cursusservice.controller;

import nl.kzaconnected.cursusservice.model.Dao.Attitude;
import nl.kzaconnected.cursusservice.service.AttitudeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(AttitudeController.class)
public class AttitudeControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private AttitudeService attitudeService;

    @Test
    public void getAllAttitudesShouldReturn200OkAndJsonArray() throws Exception {
        List<Attitude> attitudes = Arrays.asList(new Attitude(),new Attitude());
        Mockito.when(attitudeService.findAll())
                .thenReturn(attitudes);
        mockMvc.perform(get("/attitudes")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].*", hasSize(2)))
                .andExpect(jsonPath("$[1].*", hasSize(2)));
    }
}