package nl.kzaconnected.cursusservice.controller;

import nl.kzaconnected.cursusservice.model.Dao.Slagingscriterium;
import nl.kzaconnected.cursusservice.service.SlagingscriteriumService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(SlagingscriteriumController.class)
public class SlagingscriteriumControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private SlagingscriteriumService slagingscriteriumService;

    @Test
    public void getAllSlagingscriteriaShouldReturn200OkAndJsonArray() throws Exception {
        List<Slagingscriterium> slagingscriteria = Arrays.asList(new Slagingscriterium(),new Slagingscriterium());
        Mockito.when(slagingscriteriumService.findAll())
                .thenReturn(slagingscriteria);
        mockMvc.perform(get("/slagingscriteria")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].*", hasSize(2)))
                .andExpect(jsonPath("$[1].*", hasSize(2)));
    }
}