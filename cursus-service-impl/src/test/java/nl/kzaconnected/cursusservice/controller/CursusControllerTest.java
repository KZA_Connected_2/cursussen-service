package nl.kzaconnected.cursusservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.kzaconnected.cursusservice.model.Dto.CursusDto;
import nl.kzaconnected.cursusservice.service.CursusService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CursusController.class)
public class CursusControllerTest {

    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private CursusService cursusService;

    @Test
    public void getCursusByIdShouldReturn200OkAndJsonObject() throws Exception {
        CursusDto cursusDto = new CursusDto();
        Mockito.when(cursusService.findOne(1L))
                .thenReturn(cursusDto);
        mockMvc.perform(get("/cursussen/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(10)));
    }

    @Test
    public void getCursusByIdShouldReturn404NotFoundWhenNonExistent() throws Exception {
        CursusDto cursusDto = new CursusDto();
        Mockito.when(cursusService.findOne(1L))
                .thenReturn(cursusDto);
        mockMvc.perform(get("/cursussen/2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getAllCursussenShouldReturn200OkAndJsonArray() throws Exception {
        List<CursusDto> cursusDtos = Arrays.asList(new CursusDto(),new CursusDto());
        Mockito.when(cursusService.findAll())
                .thenReturn(cursusDtos);
        mockMvc.perform(get("/cursussen")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].*", hasSize(10)))
                .andExpect(jsonPath("$[1].*", hasSize(10)));
    }

    @Test
    public void postCursusShouldReturn201CreatedAndJsonObject() throws Exception {
        CursusDto cursusDto = new CursusDto();
        String requestBody = mapper.writeValueAsString(cursusDto);
        Mockito.when(cursusService.save(cursusDto))
                .thenReturn(cursusDto);
        mockMvc.perform(post("/cursussen")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.*", hasSize(10)));
    }

    @Test
    public void putCursusShouldReturn200OkAndJsonObject() throws Exception {
        CursusDto cursusDto = CursusDto.builder().id(1L).build();
        String requestBody = mapper.writeValueAsString(cursusDto);
        Mockito.when(cursusService.findOne(1L))
                .thenReturn(cursusDto);
        Mockito.when(cursusService.save(cursusDto))
                .thenReturn(cursusDto);
        mockMvc.perform(put("/cursussen/1")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(10)));
    }

    @Test
    public void putCursusShouldReturn404NotFoundWhenNonExistent() throws Exception {
        CursusDto cursusDto = CursusDto.builder().id(1L).build();
        String requestBody = mapper.writeValueAsString(cursusDto);
        Mockito.when(cursusService.findOne(1L))
                .thenReturn(cursusDto);
        Mockito.when(cursusService.save(cursusDto))
                .thenReturn(cursusDto);
        mockMvc.perform(put("/cursussen/2")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteCursusShouldReturn200OkAndJsonObject() throws Exception {
        CursusDto cursusDto = new CursusDto();
        Mockito.when(cursusService.findOne(1L))
                .thenReturn(cursusDto);
        mockMvc.perform(delete("/cursussen/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteCursusShouldReturn404NotFoundWhenNonExistent() throws Exception {
        CursusDto cursusDto = new CursusDto();
        Mockito.when(cursusService.findOne(1L))
                .thenReturn(cursusDto);
        mockMvc.perform(delete("/cursussen/2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}