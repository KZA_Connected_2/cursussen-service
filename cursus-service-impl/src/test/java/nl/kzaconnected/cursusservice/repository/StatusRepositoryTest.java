package nl.kzaconnected.cursusservice.repository;

import nl.kzaconnected.cursusservice.model.Dao.Status;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class StatusRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private StatusRepository StatusRepository;

    @Test
    public void findByStatusShouldReturnStatusWithId() {
        Status status = Status.builder().status("Status1").build();
        entityManager.persist(status);
        entityManager.flush();
        Status found = StatusRepository.findByStatus(status.getStatus());
        assertThat(found.getId()).isNotNull();
        assertThat(found.getStatus()).isEqualTo(status.getStatus());
    }
}