package nl.kzaconnected.cursusservice.controller;

import nl.kzaconnected.cursusservice.model.Dao.Status;
import nl.kzaconnected.cursusservice.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/statussen")
public class StatusController {

    @Autowired
    private StatusService statusService;

    @GetMapping
    public List<Status> getAllStatussen() {
        return statusService.findAll();
    }
}


