package nl.kzaconnected.cursusservice.controller;

import nl.kzaconnected.cursusservice.model.Dto.CursusDto;
import nl.kzaconnected.cursusservice.service.CursusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/cursussen")
public class CursusController {

    @Autowired
    private CursusService cursusService;

    @GetMapping("/{id}")
    public ResponseEntity<CursusDto> getCursusById(@PathVariable(value = "id") Long id) {
        CursusDto cursus = cursusService.findOne(id);
        if(cursus == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(cursus);
    }

    @GetMapping
    public ResponseEntity<List<CursusDto>> getAllCursussen() {
        return ResponseEntity.ok(cursusService.findAll());
    }

    @PostMapping
    public ResponseEntity<CursusDto> createCursus(@Valid @RequestBody CursusDto cursusDto) {
        cursusDto.setId(null);
        CursusDto cursusDtoResponse = cursusService.save(cursusDto);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(cursusDtoResponse.getId()).toUri();

        return ResponseEntity.created(location).body(cursusDtoResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CursusDto> updateCursus(@PathVariable(value = "id") Long id,@Valid @RequestBody CursusDto cursus) {
        cursus.setId(id);

        if(cursusService.findOne(id) == null) {
            return ResponseEntity.notFound().build();
        }

        CursusDto updatedCursus = cursusService.save(cursus);
        return ResponseEntity.ok(updatedCursus);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<CursusDto> deleteCursus(@PathVariable(value = "id") Long id) {
        CursusDto cursus = cursusService.findOne(id);
        if(cursus == null) {
            return ResponseEntity.notFound().build();
        }

        cursusService.delete(id);
        return ResponseEntity.ok().build();
    }
}