package nl.kzaconnected.cursusservice.controller;

import nl.kzaconnected.cursusservice.model.Dao.Slagingscriterium;
import nl.kzaconnected.cursusservice.service.SlagingscriteriumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/slagingscriteria")
public class SlagingscriteriumController {

    @Autowired
    private SlagingscriteriumService slagingscriteriumService;

    @GetMapping
    public List<Slagingscriterium> getAllSlagingscriteria() {
        return slagingscriteriumService.findAll();
    }
}


