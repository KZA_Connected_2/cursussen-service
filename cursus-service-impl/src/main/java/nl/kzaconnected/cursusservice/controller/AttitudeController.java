package nl.kzaconnected.cursusservice.controller;

import nl.kzaconnected.cursusservice.model.Dao.Attitude;
import nl.kzaconnected.cursusservice.service.AttitudeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/attitudes")
public class AttitudeController {

    @Autowired
    private AttitudeService attitudeService;

    @GetMapping
    public List<Attitude> getAllAttitudes() {
        return attitudeService.findAll();
    }
}


