package nl.kzaconnected.cursusservice.controller;

import nl.kzaconnected.cursusservice.model.Dao.Functieniveau;
import nl.kzaconnected.cursusservice.service.FunctieniveauService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/functieniveaus")
public class FunctieniveauController {

    @Autowired
    private FunctieniveauService functieniveauService;

    @GetMapping
    public List<Functieniveau> getAllfunctieniveaus() {
        return functieniveauService.findAll();
    }
}


