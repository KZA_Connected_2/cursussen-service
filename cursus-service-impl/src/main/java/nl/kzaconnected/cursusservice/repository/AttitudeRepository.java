package nl.kzaconnected.cursusservice.repository;

import nl.kzaconnected.cursusservice.model.Dao.Attitude;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AttitudeRepository extends JpaRepository<Attitude,Long> {

    public Attitude findByAttitude(String attitude);
}
