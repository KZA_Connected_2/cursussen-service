package nl.kzaconnected.cursusservice.repository;

import nl.kzaconnected.cursusservice.model.Dao.Slagingscriterium;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SlagingscriteriumRepository extends JpaRepository<Slagingscriterium,Long> {

    public Slagingscriterium findBySlagingscriterium(String slagingscriterium);
}
