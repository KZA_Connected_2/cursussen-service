package nl.kzaconnected.cursusservice.service;

import nl.kzaconnected.cursusservice.model.Dto.CursusDto;
import nl.kzaconnected.cursusservice.model.Dao.Cursus;
import nl.kzaconnected.cursusservice.repository.*;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Type;
import java.util.List;

public class AbstractCursusService {

    @Autowired
    protected AttitudeRepository attitudeRepository;
    @Autowired
    protected FunctieniveauRepository functieniveauRepository;
    @Autowired
    protected SlagingscriteriumRepository slagingscriteriumRepository;
    @Autowired
    protected StatusRepository statusRepository;

    protected Cursus convertToDao(CursusDto cursusDto){
        return new ModelMapper()
                .map(cursusDto,Cursus.class);
    }

    protected CursusDto convertToDto(Cursus cursus){
        return new ModelMapper()
                .map(cursus,CursusDto.class);
    }

    protected List<CursusDto> convertToDtoList(List<Cursus> cursussen){
        Type cursusDtoListType = new TypeToken<List<CursusDto>>() {}.getType();
        return new ModelMapper()
                .map(cursussen, cursusDtoListType);
    }
}
