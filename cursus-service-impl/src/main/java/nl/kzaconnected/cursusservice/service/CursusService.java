package nl.kzaconnected.cursusservice.service;

import nl.kzaconnected.cursusservice.model.Dto.CursusDto;
import nl.kzaconnected.cursusservice.model.Dao.*;
import nl.kzaconnected.cursusservice.repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CursusService extends AbstractCursusService {

    @Autowired
    private CursusRepository cursusRepository;

    public CursusDto findOne(Long id) {
        Cursus cursus = cursusRepository.findOne(id);
        return convertToDto(cursus);
    }

    public List<CursusDto> findAll() {
        List<Cursus> cursussen = cursusRepository.findAll();
        return convertToDtoList(cursussen);
    }

    public CursusDto save(CursusDto cursusDto){
        Attitude attitude = attitudeRepository.findByAttitude(cursusDto.getAttitude());
        Functieniveau functieniveau = functieniveauRepository.findByFunctieniveau(cursusDto.getFunctieniveau());
        Slagingscriterium slagingscriterium = slagingscriteriumRepository.findBySlagingscriterium(cursusDto.getSlagingscriterium());
        Status status = statusRepository.findByStatus(cursusDto.getStatus());
        Cursus cursus = convertToDao(cursusDto);
        cursus.getAttitude().setId(attitude.getId());
        cursus.getFunctieniveau().setId(functieniveau.getId());
        cursus.getSlagingscriterium().setId(slagingscriterium.getId());
        cursus.getStatus().setId(status.getId());
        cursus = cursusRepository.save(cursus);
        return convertToDto(cursus);
    }

    public void delete(Long id){cursusRepository.delete(id);}
}
