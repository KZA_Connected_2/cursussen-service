package nl.kzaconnected.cursusservice.service;

import nl.kzaconnected.cursusservice.model.Dao.Attitude;
import nl.kzaconnected.cursusservice.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AttitudeService {

    @Autowired
    private AttitudeRepository attitudeRepository;

    public List<Attitude> findAll(){
        return attitudeRepository.findAll();
    }
}
