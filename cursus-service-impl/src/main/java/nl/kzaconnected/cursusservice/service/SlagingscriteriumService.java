package nl.kzaconnected.cursusservice.service;

import nl.kzaconnected.cursusservice.model.Dao.Slagingscriterium;
import nl.kzaconnected.cursusservice.repository.SlagingscriteriumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SlagingscriteriumService {

    @Autowired
    private SlagingscriteriumRepository slagingscriteriumRepository;

    public List<Slagingscriterium> findAll(){
        return slagingscriteriumRepository.findAll();
    }
}
