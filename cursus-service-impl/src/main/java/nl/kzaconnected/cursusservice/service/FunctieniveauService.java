package nl.kzaconnected.cursusservice.service;

import nl.kzaconnected.cursusservice.model.Dao.Functieniveau;
import nl.kzaconnected.cursusservice.repository.FunctieniveauRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FunctieniveauService {

    @Autowired
    private FunctieniveauRepository functieniveauRepository;

    public List<Functieniveau> findAll(){
        return functieniveauRepository.findAll();
    }
}
