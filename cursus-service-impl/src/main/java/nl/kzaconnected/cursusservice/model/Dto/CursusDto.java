package nl.kzaconnected.cursusservice.model.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.kzaconnected.cursusservice.model.Dao.Datum;
import nl.kzaconnected.cursusservice.model.Dao.Docent;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CursusDto {

    private Long id;
    private String naam;
    private String attitude;
    private String functieniveau;
    private String slagingscriterium;
    private String status;
    private int maxDeelnemers;
    private String beschrijving;
    private List<Datum> cursusdata;
    private List<Docent> cursusdocenten;
}
