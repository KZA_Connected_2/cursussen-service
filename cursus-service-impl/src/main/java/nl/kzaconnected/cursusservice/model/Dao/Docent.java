package nl.kzaconnected.cursusservice.model.Dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "CURSUSDOCENTEN")
public class Docent implements Serializable {

    @Id
    @JsonIgnore
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAAM")
    private String naam;
}