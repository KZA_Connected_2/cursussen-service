package nl.kzaconnected.cursusservice.model.Dao;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "CURSUSDATA")
public class Datum implements Serializable {

    @Id
    @JsonIgnore
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @Column(name = "DATUM")
    private Date datum;
}