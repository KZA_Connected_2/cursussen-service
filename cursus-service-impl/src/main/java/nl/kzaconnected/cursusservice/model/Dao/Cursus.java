package nl.kzaconnected.cursusservice.model.Dao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "CURSUSSEN")
public class Cursus implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @NotNull
    @Column(name = "NAAM")
    private String naam;

    @NotNull
    @ManyToOne
    @JoinColumn(name="ATTITUDE_ID")
    private Attitude attitude;

    @NotNull
    @ManyToOne
    @JoinColumn(name="FUNCTIENIVEAU_ID")
    private Functieniveau functieniveau;

    @NotNull
    @ManyToOne
    @JoinColumn(name="SLAGINGSCRITERIUM_ID")
    private Slagingscriterium slagingscriterium;

    @NotNull
    @ManyToOne
    @JoinColumn(name="STATUS_ID")
    private Status status;

    @NotNull
    @Column(name = "MAXDEELNEMERS")
    private int maxDeelnemers;

    @NotNull
    @Column(name = "BESCHRIJVING")
    private String beschrijving;

    @OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name = "CURSUS_ID")
    private List<Datum> cursusdata;

    @OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name = "CURSUS_ID")
    private List<Docent> cursusdocenten;
}